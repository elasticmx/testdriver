using System;

namespace TestDriver
{
public class Tester
{
    public static void main(string[] args)
    {
        Console.WriteLine("Hello World!");

        string[] tester = new string[] {"Hi","Tester"};
        foreach(var a in tester) {
            Console.WriteLine($"{a}");
        }
    }

}

}